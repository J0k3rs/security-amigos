package com.jokers.security.student;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("management/api/v1/students")
public class StudentManagmentController {

    private static final List<Student> STUDENTS = Arrays.asList(
            new Student(1L,"Oumar Keita"),
            new Student(2L,"Lassana Traore"),
            new Student(3L,"Sidiki Diaby"),
            new Student(4L,"Moussa Cisse")
    );

    @GetMapping
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_ADMINTRAINEE')")
    public List<Student> getAllStudents(){
        System.out.println("List of all Student");
        return STUDENTS;
    }

    @PostMapping
    @PreAuthorize("hasAuthority('student:write')")
    public void registerStudent(@RequestBody Student student){
        System.out.println("Register new Student");
        System.out.println(student);
    }

    @DeleteMapping(path = "{studentId}")
    @PreAuthorize("hasAuthority('student:write')")
    public void deleteStudent(@PathVariable("studentId") Long studentId){
        System.out.println("Delete Student");
        System.out.println(studentId);
    }

    @PutMapping(path = "{studentId}")
    @PreAuthorize("hasAuthority('student:write')")
    public void updateStudent(@PathVariable("studentId") Long studentId, @RequestBody  Student student){
        System.out.println("Updates Student");
        System.out.println(String.format("%s %s", studentId, student));
    }
}
